#include "router_node.h"

std::list<RouterNode> RouterNode::fromFile(std::string path) {
  std::list<RouterNode> nodes;
  std::ifstream stream(path);

  if (stream.good()) {
    std::string line;
    while (std::getline(stream, line)) {
      // read ID; PORT; NEIGHBOR_1,NEIGHBOR_2,...
      std::vector<std::string> entries = router::split(line, ';');
      int id = std::stoi(entries[0]);
      unsigned int port = std::stoi(entries[1]);

      std::list<int> connectedRouters;
      std::vector<std::string> neighbors = router::split(entries[2], ','); // split neighbor router ids
      for (auto &neighbor : neighbors) {
        connectedRouters.push_back(std::stoi(neighbor));
      }

      nodes.push_back(RouterNode(id, port, connectedRouters));
    }
  }

  return nodes;
}

RouterNode::RouterNode(int id, unsigned short port, std::list<int> neighborRouters)
    : id(id), port(port), neighborRouters(neighborRouters) {
}

int RouterNode::getId() const {
  return id;
}

unsigned short RouterNode::getPort() const {
  return port;
}

const std::list<int> &RouterNode::getNeighborRouters() const {
  return neighborRouters;
}
std::ostream &operator<<(std::ostream &o, RouterNode &node) {
  // join connected routers
  std::stringstream res;
  std::copy(node.neighborRouters.begin(), node.neighborRouters.end(), std::ostream_iterator<int>(res, ","));
  std::string routers = res.str();
  routers.pop_back(); // remove last , from string

  o << "Id: " << node.getId() << "; Port: " << node.getPort() << "; Connected Routers: " << routers;
  return o;
}

RouterNode::RouterNode(RouterNode &other) {
  id = other.id;
  port = other.port;
  neighborRouters = other.neighborRouters;
}

RouterNode::RouterNode(RouterNode &&other) {
  id = other.id;
  port = other.port;
  neighborRouters = other.neighborRouters;
}

RouterNode &RouterNode::operator=(RouterNode &other) {
  id = other.id;
  port = other.port;
  neighborRouters = other.neighborRouters;

  return *this;
}

RouterNode::RouterNode() {

}


