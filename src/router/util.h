#ifndef ROUTING_ROUTER_UTIL_H_
#define ROUTING_ROUTER_UTIL_H_

#include <vector>
#include <string>
#include <sstream>
#include <iterator>
#include <algorithm>
#include <csignal>
#include <functional>

namespace router {

/**
 * Splits this string around the given delimiter
 * @param s string to split
 * @param delimiter the delimiter
 * @return vector of all strings computed by splitting the string around the delimiter
 */
std::vector<std::string> split(const std::string &s, char delimiter);


void signalHandler(int signum); // wrapper for signalHandlerFunction to be accepted by signal

/**
 * Add a signal handler for the given signum
 *
 * @see csignal
 * @param signum signal to handle
 * @param handler handler function
 */
void handleSignal(int signum, std::function<void(int)> handler);

}
#endif //ROUTING_ROUTER_UTIL_H_
