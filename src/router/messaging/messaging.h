#ifndef ROUTING_ROUTER_MESSAGING_MESSAGING_H_
#define ROUTING_ROUTER_MESSAGING_MESSAGING_H_

#include <string>
#include <iostream>
#include <thread>
#include <atomic>

#include "server/udp/udp_server_socket.h"
#include "../util.h"
#include "../router.h"

const int PORT_OFFSET = 1000;

class Messaging {
 public:

  /**
   * Create a new messaging instance
   * @param port port to listen on for messages
   * @param thisRouter id of this router
   * @param router this router instance
   * @param routers other routers present in the network
   */
  Messaging(unsigned short port, int thisRouter, Router& router, std::list<RouterNode> &routers);

  /**
   * Destructor for Messaging
   */
  ~Messaging();

  /**
   * Start listening for incoming messages
   */
  void startListening();

  /**
   * Send a new message to a given destination
   * @param destination target router id
   * @param message message to be sent
   */
  void sendMessage(int destination, std::string message);

  /**
   * Forward message to the next hop
   * @param from origin of the message
   * @param to next hop to reach the message destination
   * @param id unique id given to the message by the sending router
   * @param message message to be forwarded
   */
  void forwardMessage(int &from, int &to, int &id, std::string &message);

 private:
  int messageCounter;
  int thisRouter;
  UDPServerSocket server;

  std::atomic<bool> isListening;
  std::thread listeningLoop;

  Router& router;
  std::list<RouterNode>& routers;
};

#endif
