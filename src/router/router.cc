#include "router.h"

using namespace std::chrono_literals;
using namespace std::chrono;

Router::Router(RouterNode configuration, std::list<RouterNode> &otherRouters)
    : thisRouter(configuration),
      server(configuration.getPort()),
      table(configuration.getId(), configuration.getNeighborRouters()),
      isListening(false),
      isPublishing(false),
      lastPublish(0) {
  for (auto router : otherRouters) {
    if (router.getId() == configuration.getId()) {
      continue;
    }
    routers[router.getId()] = router;
  }

  server.start();
  startRemovalLoop();
}

Router::~Router() {
  server.stopGracefully();
  if (isRemovalLoopRunning) {
    isRemovalLoopRunning = false;
    removalLoopThread.join();
  }
}

void Router::startListening() {
  if (isListening) {
    return;
  }

  listeningThread = std::thread([this]() {
    isListening = true;
    while (isListening) {
      std::optional<IncomingMessage> message = server.receiveNextMessage();
      if (message.has_value()) {
        std::vector<std::string> packetData = router::split(message->getMessage(), ';');
        std::string packetType = packetData[0];

        if (packetType == "ROUTE") {
          handleRoutePacket(packetData);
        } else if (packetType == "ALIVE") {
          handleAlivePacket(packetData);
        }
      }
    }
  });
}

void Router::handleRoutePacket(std::vector<std::string> &packetData) {
  int otherRouterId;
  try {
    otherRouterId = std::stoi(packetData[2]);
  } catch (std::invalid_argument&) {
    std::cerr << "Received invalid router id" << std::endl;
    return;
  }

  if (routers.find(otherRouterId) == routers.end()) {
    std::cerr << "Received packet from unknown router: " << otherRouterId << std::endl;
    return;
  }

  // update routing table
  table.processMessage(packetData);
}

void Router::publishInformationPeriodically(int periodSeconds) {
  if (isPublishing) {
    return;
  }

  publishThread = std::thread([this, periodSeconds]() {
    isPublishing = true;

    while (isPublishing) {
      auto now = duration_cast<milliseconds>(system_clock::now().time_since_epoch());
      auto diff = duration_cast<seconds>(now - lastPublish);

      // send table if the table has never been published before
      // or the last publish was periodSeconds ago
      if (lastPublish.count() == 0 || diff.count() == periodSeconds) {
        for (auto &neighbor : thisRouter.getNeighborRouters()) {
          sendAlivePacket(routers[neighbor]);
          sendRoutingTable(routers[neighbor]);
        }
        lastPublish = now;
      }

      std::this_thread::sleep_for(500ms);
    }
  });
}

void Router::startRemovalLoop() {
  removalLoopThread = std::thread([this]() {
    isRemovalLoopRunning = true;

    while (isRemovalLoopRunning) {
      std::this_thread::sleep_for(1s);
      table.removeInactiveRoutes();
    }
  });
}

void Router::sendAlivePacket(RouterNode &target) {
  UDPClient client("localhost", target.getPort());

  try {
    std::string message = "ALIVE;" + std::to_string(thisRouter.getId());
    client << message;
  } catch (SocketException &ex) {
    std::cout << "Couldn't send routing table to router: " << target.getId() << std::endl;
  }

  client.stop();
}

void Router::handleAlivePacket(std::vector<std::string> &packetData) {
  int otherRouterId;
  try {
    otherRouterId = std::stoi(packetData[1]);
  } catch (std::invalid_argument&) {
    std::cerr << "Received invalid router id" << std::endl;
    return;
  }

  if (routers.find(otherRouterId) == routers.end()) {
    std::cerr << "Received packet from unknown router: " << otherRouterId << std::endl;
    return;
  }

  // add connected router to route table
  table.addOrUpdateRoute(otherRouterId, otherRouterId, 1);
}

void Router::sendRoutingTable(RouterNode &target) {
  UDPClient client("localhost", target.getPort());

  try {
    table.sendRouteTable(client);
  } catch (SocketException &ex) {
    std::cout << "Couldn't send routing table to router: " << target.getId() << std::endl;
  }

  client.stop();
}

void Router::stopListening() {
  if (!isListening) {
    return;
  }

  isListening = false;
  listeningThread.join();
}

void Router::stopPublishing() {
  if (!isPublishing) {
    return;
  }

  isPublishing = false;
  publishThread.join();
}

int Router::getNextRouter(int destination) {
  return table.getNextHop(destination); // propagate call to router
}

