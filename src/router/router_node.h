#ifndef ROUTING_ROUTER_ROUTER_NODE_H_
#define ROUTING_ROUTER_ROUTER_NODE_H_

#include <iostream>
#include <string>
#include <list>
#include <fstream>
#include <algorithm>

#include "util.h"

/**
 * Configuration of a router
 */
class RouterNode {
 public:
  /**
   * Create a new Router Node
   */
  RouterNode();
  /**
   * Create a new Router Node
   * @param id id of the router
   * @param port port to contact the router
   * @param neighborRouters neighbors of the router (ids)
   */
  RouterNode(int id, unsigned short port, std::list<int> neighborRouters);

  /**
   * Copy constructor for RouterNode
   * @param other the other router
   */
  RouterNode(RouterNode& other);

  /**
   * Move constructor for RouterNode
   * @param other the other router
   */
  RouterNode(RouterNode&& other);

  /**
   * Copy operator= for Router
   * @param other the other router
   * @return this router
   */
  RouterNode& operator=(RouterNode& other);

  /**
   * Get the id of the current router
   * @return id
   */
  int getId() const;

  /**
   * Get the port of the current router
   * @return port
   */
  unsigned short getPort() const;

  /**
   * Get neighbors (connected routers) of the current router
   * @return ids of the neighbors
   */
  const std::list<int> &getNeighborRouters() const;

  /**
   * Read router configurations from a file
   * @param path path to a configuration file
   * @return list of router configurations
   */
  static std::list<RouterNode> fromFile(std::string path);

  friend std::ostream& operator<<(std::ostream &o, RouterNode& node);
 private:
  int id;
  unsigned short port;
  std::list<int> neighborRouters;

};

#endif
