#include "server/socket.h"
#include "router_node.h"
#include "router.h"
#include "messaging/messaging.h"

/**
 * Start input loop from stdin
 * This loop exits when "exit" is typed to stdin
 */
void startInputLoop(Messaging &messaging) {
  std::string line;
  while (std::getline(std::cin, line)) {
    if (line == "exit") {
      break;
    } else if (line.rfind("send", 0) == 0) {
      std::vector<std::string> args = router::split(line, ' ');
      if (args.size() < 3) {
        std::cout << "Usage: send <destination> <message> [message...]" << std::endl;
      }

      int destination = std::stoi(args[1]);
      std::stringstream message;

      // concat all arguments after <destination>
      for (std::string::size_type i = 2; i < args.size(); ++i) {
        message << args[i] << " ";
      }

      messaging.sendMessage(destination, message.str());
    } else {
      std::cout << "Unknown command: " << line << std::endl;
    }
  }
}

/**
 * Entrypoint for the router
 *
 * Usage: ./router <b>id</b>
 * @param argc expected arguments: 1
 * @param argv first argument should be the id of a router in the router.config file
 * @return
 */
int main(int argc, char **argv) {
  // convert argv to strings
  std::vector<std::string> args;
  args.assign(argv, argv + argc);

  if (args.size() < 2) {
    std::cerr << "Please provide a router id as the first parameter" << std::endl;
    return 1;
  }

  std::list<RouterNode> nodes = RouterNode::fromFile("router.config");
  std::cout << "Known routers: " << std::endl;
  for (auto &it : nodes) {
    std::cout << "  " << it << std::endl;
  }

  int routerId;
  try {
    routerId = std::stoi(args[1]);
  } catch (std::invalid_argument& ex) {
    std::cerr << "Please provide a number as the first argument" << std::endl;
    return 1;
  }

  // find matching router for given id
  auto foundRouterNode =
      std::find_if(nodes.begin(), nodes.end(), [&](RouterNode &it) { return it.getId() == routerId; });
  if (foundRouterNode == nodes.end()) {
    std::cerr << "Couldn't find configuration for router with id: " << routerId << std::endl;
    return 1;
  } else {
    std::cout << "Current configuration set to: " << *foundRouterNode << std::endl;
  }

  // start router with matching router configuration
  Router router(*foundRouterNode, nodes);
  Messaging messaging(foundRouterNode->getPort() + PORT_OFFSET, foundRouterNode->getId(), router, nodes);

  router.startListening();
  router.publishInformationPeriodically();
  messaging.startListening();

  std::cout << "Listening for incoming messages" << std::endl;

  // add handler for Ctrl+C
  router::handleSignal(SIGINT, [&](int) {
    std::cout << "Received SIGINT" << std::endl;
    router.stopListening();
    router.stopPublishing();
    std::exit(0);
  });

  startInputLoop(messaging);

  std::cout << "Stopping listening loop..." << std::endl;
  router.stopListening();
  router.stopPublishing();
  return 0;
}