#include <variadic_table.h>
#include "route_table.h"

using namespace std::chrono;
using namespace std::chrono_literals;

RouteTable::RouteTable(int thisRouter, const std::list<int> &directNeighbors) : thisRouter(thisRouter) {
}

void RouteTable::processMessage(std::vector<std::string> &args) {
  int to = std::stoi(args[1]);
  if (to == thisRouter) { // ignore own routing information
    return;
  }
  int from = std::stoi(args[2]);
  int hopCount = std::stoi(args[3]);

  addOrUpdateRoute(from, to, hopCount + 1);
}

void RouteTable::sendRouteTable(UDPClient &client) {
  std::stringstream stream;

  std::lock_guard<std::mutex> guard(routesMutex);
  for (auto it = routes.begin(); it != routes.end(); ++it) {
    stream.str("");
    auto &route = it->second;

    stream << "ROUTE;" << route.destination << ";" << thisRouter << ";" << route.hopCount;
    std::string message = stream.str();
    client << message;
  }
}

void RouteTable::removeInactiveRoutes() {
  std::lock_guard<std::mutex> guard(routesMutex);

  bool tableChanged = false;

  auto now = duration_cast<milliseconds>(system_clock::now().time_since_epoch());
  for (auto it = routes.begin(); it != routes.end();) {
    auto &route = it->second;
    auto diff = duration_cast<seconds>(now - route.lastUpdated);
    if (diff >= 30s) {
      std::cout << "Removing destination " << route.destination << " because of inactivity (" << diff.count() << "s)"
                << std::endl;
      tableChanged = true;
      it = routes.erase(it);
    } else {
      ++it;
    }
  }

  if (tableChanged) {
    printRouteTable();
  }
}

void RouteTable::addOrUpdateRoute(const int &from, const int &to, const int &hopCount) {
  std::lock_guard<std::mutex> guard(routesMutex);

  auto now = duration_cast<milliseconds>(system_clock::now().time_since_epoch());
  auto existingRoute = routes.find(to);
  if (existingRoute == routes.end()) {
    std::cout << "Adding new route to router " << to << std::endl;
    routes[to] =
        {.destination = to, .nextHop = from, .hopCount = hopCount, .lastUpdated = now};
    printRouteTable();
  } else {
    auto &route = existingRoute->second;

    // update existing route if the announced route has a lower hopCount than the current route
    if (hopCount < route.hopCount) {
      route.hopCount = hopCount;
      route.nextHop = from;
      printRouteTable();
    }
    route.lastUpdated = now;
  }
}

void RouteTable::printRouteTable() {
  if (routes.size() == 0) {
    std::cout << "There are no known routes." << std::endl;
    return;
  }

  std::cout << "\nCurrent known routes:" << std::endl;

  VariadicTable<int, int, int> table({"Destination", "Next Hop", "Hop Count"});
  for (auto it = routes.begin(); it != routes.end(); ++it) {
    auto &route = it->second;
    table.addRow(route.destination, route.nextHop, route.hopCount);
  }
  table.print(std::cout);
}

int RouteTable::getNextHop(int destination) {
  std::lock_guard<std::mutex> guard(routesMutex);

  auto route = routes.find(destination);
  if (route == routes.end()) {
    return -1;
  }
  return route->second.nextHop;
}

