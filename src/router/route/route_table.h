#ifndef ROUTING_ROUTER_ROUTE_TABLE_H_
#define ROUTING_ROUTER_ROUTE_TABLE_H_

#include <mutex>
#include <map>
#include <vector>
#include <string>
#include <sstream>
#include <algorithm>
#include <list>

#include "route.h"
#include "client/udp/client.h"

class RouteTable {
 public:
  /**
   * Create a new Router Table
   * @param thisRouter id of this router
   * @param directNeighbors neighbors of this router
   */
  RouteTable(int thisRouter, const std::list<int> &directNeighbors);

  /**
   * Process incoming ROUTE message
   *
   * @see Router::handleRoutePacket for more information
   * @param args
   */
  void processMessage(std::vector<std::string> &args);

  /**
   * Send the current route table to the given client
   * @param client client to send the table to
   */
  void sendRouteTable(UDPClient &client);

  /**
   * Remove routes that haven't been updated in >=30 seconds
   */
  void removeInactiveRoutes();

  /**
   * Get next hop to destination router
   * @param destination id of destination router
   * @return next hop if destination exists in table, else -1
   */
  int getNextHop(int destination);

  /**
   * Add new route or update route if already present
   * @param from source router
   * @param to destination router
   * @param hopCount hops until destination router
   */
  void addOrUpdateRoute(const int &from, const int &to, const int &hopCount);
 private:
  int thisRouter;

  std::mutex routesMutex; // lock for routes map
  std::map<int, Route> routes;

  /**
   * Print the current known routes as a table
   */
  void printRouteTable();
};

#endif
