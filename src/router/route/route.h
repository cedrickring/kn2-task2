#ifndef ROUTING_ROUTER_ROUTE_H_
#define ROUTING_ROUTER_ROUTE_H_

#include <chrono>

struct Route {
  int destination; // id of destination router
  int nextHop; // id of the next router
  int hopCount; // hops to the destination
  std::chrono::milliseconds lastUpdated; // timestamp of last update
  //bool isDirectNeighbor; // true, if the destination is a direct neighbor of the actual router
};

#endif //ROUTING_ROUTER_ROUTE_H_
