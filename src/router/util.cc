#include "util.h"

std::function<void(int)> signalHandlerFunction;

std::vector<std::string> router::split(const std::string &s, char delimiter) {
  std::vector<std::string> tokens;
  std::string token;
  std::istringstream tokenStream(s);
  while (std::getline(tokenStream, token, delimiter)) {
    tokens.push_back(token);
  }
  return tokens;
}

void router::handleSignal(int signum, std::function<void(int)> handler) {
  signalHandlerFunction = handler;
  signal(signum, signalHandler);
}

void router::signalHandler(int signum) {
  signalHandlerFunction(signum);
}

