#ifndef ROUTING_ROUTER_ROUTER_H_
#define ROUTING_ROUTER_ROUTER_H_

#include <map>
#include <list>
#include <thread>
#include <atomic>
#include <chrono>
#include <mutex>

#include "router_node.h"
#include "route/route_table.h"
#include "server/udp/udp_server_socket.h"
#include "client/udp/client.h"
#include "variadic_table.h"

class Router {
 public:
  /**
   * Construct a new Router with given configuration and neighbor routers
   * @param configuration configuration for the router
   * @param otherRouters neighbors of the router
   */
  Router(RouterNode configuration, std::list<RouterNode> &otherRouters);

  /**
   * Destructor for router
   */
  ~Router();

  /**
   * Start listening loop for incoming messages from other routers
   */
  void startListening();

  /**
   * Stop listening loop created in <b>startListening()</b>
   */
  void stopListening();

  /**
   * Start publishing loop of routing information to other routers
   * @param periodSeconds the period between publishes (in seconds), defaults to 10 seconds
   */
  void publishInformationPeriodically(int periodSeconds = 10);

  /**
   * Stop publishing loop created in <b>publishInformationPeriodically(int)</b>
   */
  void stopPublishing();

  /**
   * Get next hop to get to the destination
   * @param destination id of destination router
   * @return next hop
   */
  int getNextRouter(int destination);

 private:
  RouterNode thisRouter;
  UDPServerSocket server;
  std::map<int, RouterNode> routers; // known routers from this router

  RouteTable table;

  std::thread listeningThread;
  std::atomic<bool> isListening;

  std::atomic<bool> isPublishing;
  std::thread publishThread;
  std::chrono::milliseconds lastPublish; // keep track of last publish to know when to publish next

  std::thread removalLoopThread;
  std::atomic<bool> isRemovalLoopRunning;

  /**
   * Send routing table to another router
   * @param target the router to send the table to
   */
  void sendRoutingTable(RouterNode &target);

  /**
   * Send alive packet to another router
   * @param target the router to send the table to
   */
  void sendAlivePacket(RouterNode &target);

  /**
   * Start internal removal loop for periodic route table cleaning
   */
  void startRemovalLoop();

  /**
   * Handle incoming ROUTE packet
   *
   * format should be <b>ROUTE;TO;FROM;HOP_COUNT</b>
   * <br>
   * e.g. ROUTE;1;2;1
   * @param packetData packet arguments
   */
  void handleRoutePacket(std::vector<std::string> &packetData);

  /**
   * Handle incoming ALIVE packet
   *
   * format should be <b>ALIVE;FROM<b>
   * <br>
   * e.g. ALIVE;1
   * @param packetData packet arguments
   */
  void handleAlivePacket(std::vector<std::string> &packetData);

};

#endif
