#ifndef ROUTING_SOCKETS_COMMON_UTIL_H_
#define ROUTING_SOCKETS_COMMON_UTIL_H_

#include <netdb.h>
#include <string>
#include <cstring>
#include "exception.h"

namespace sockets {

/**
 * Perform a dns lookup for a given host & port
 * @param host foreign host
 * @param port foreign port
 * @param result pointer to an addrinfo where the results are being placed in
 */
void lookupHost(std::string host, unsigned short port, addrinfo **result);

}

#endif //ROUTING_SOCKETS_COMMON_UTIL_H_
