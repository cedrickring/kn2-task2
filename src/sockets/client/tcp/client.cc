#include "client.h"

TCPClient::TCPClient(std::string host, unsigned short port) : host(std::move(host)), port(port) {
}

void TCPClient::connectToServer() {
  addrinfo* result;
  sockets::lookupHost(host, port, &result);

  for (addrinfo* info = result; info != NULL; info = info->ai_next) {
    // try to connect to found ip
    fd = socket(info->ai_family, info->ai_socktype, info->ai_protocol);
    if (fd == -1) {
      continue;
    }

    if (connect(fd, info->ai_addr, info->ai_addrlen) == -1) {
      fd = -1;
      continue;
    }
  }

  if (fd == -1) {
    throw UnknownHostException(host, port);
  }

  Receiver::setFd(fd);
  Sender::setFd(fd);
}

void TCPClient::stop() {
  if (fd != -1) {
    close(fd);
  }
}

bool TCPClient::isConnected() {
  return fd != -1;
}
