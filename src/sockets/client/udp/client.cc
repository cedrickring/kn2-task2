#include "client.h"

UDPClient::UDPClient(std::string host, unsigned short port) : host(std::move(host)), port(port) {
  if ((fd = socket(AF_INET, SOCK_DGRAM, 0)) == -1) {
    throw SocketException("Unable to open udp server");
  }

  sockets::lookupHost(this->host, this->port, &info);
}

UDPClient &UDPClient::operator<<(const char *message) {
  std::string messageStr = message;
  return operator<<(messageStr);
}

UDPClient &UDPClient::operator<<(std::string &message) {
  if (sendto(fd, message.c_str(), message.length(), 0, info->ai_addr, info->ai_addrlen) == -1) {
    throw SocketException("Failed to send message to " + host + ":" + std::to_string(port));
  }

  return *this;
}

UDPClient &UDPClient::operator>>(std::string &output) {
  output.clear();

  const unsigned int MAX_BUF_LENGTH = 4096;
  std::vector<char> buffer(MAX_BUF_LENGTH);

  int read = recvfrom(fd, &buffer[0], MAX_BUF_LENGTH, 0, NULL, 0);
  if (read == -1) {
    throw SocketException("Failed to read from udp server");
  }

  output.append(buffer.cbegin(), buffer.cend());
  return *this;
}

void UDPClient::stop() {
  if (fd != -1) {
    close(fd);
  }
}

