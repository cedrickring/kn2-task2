#ifndef ROUTING_SOCKETS_SERVER_UDP_INCOMING_MESSAGE_H_
#define ROUTING_SOCKETS_SERVER_UDP_INCOMING_MESSAGE_H_

#include <string>

class IncomingMessage {
 public:
  /**
   * Create a IncomingMessage instance containing the actual message and connection information
   * @param message actual message
   * @param remoteHost host of sender
   * @param remotePort port of sender
   */
  explicit IncomingMessage(std::string message, std::string remoteHost, unsigned int remotePort);

  /**
   * Get the actual message
   * @return message
   */
  const std::string &getMessage() const;

  /**
   * Get the host of the message sender
   * @return host of sender
   */
  const std::string &getRemoteHost() const;

  /**
   * Get the port of the message sender
   * @return port of sender
   */
  unsigned short getRemotePort() const;

 private:
  std::string message;
  std::string remoteHost;
  unsigned short remotePort;
};

#endif
