#include "incoming_message.h"

IncomingMessage::IncomingMessage(std::string message,
                                 std::string remoteHost,
                                 unsigned int remotePort)
    : message(std::move(message)), remoteHost(std::move(remoteHost)), remotePort(remotePort) {
}

const std::string &IncomingMessage::getMessage() const {
  return message;
}

const std::string &IncomingMessage::getRemoteHost() const {
  return remoteHost;
}

unsigned short IncomingMessage::getRemotePort() const {
  return remotePort;
}