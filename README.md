# Assignment 2: Routing

## Prerequisites

* Linux/Unix with POSIX-style sockets and pthreads (libstdc++, libpthread, libgcc), tested on Ubuntu 20.04
* CMake 3.10+
* G++ with C++17 Support (I'm using 9.3.0)
* (Optional) Ninja for building

**NOTE**: g++-7 won't be able to compile this code!

To install g++-9 use:

```
sudo add-apt-repository ppa:ubuntu-toolchain-r/test
sudo apt update
sudo apt install gcc-9 g++-9
```

And set `CC` and `CXX` to the correct compiler before running `cmake`:
```
export CC=gcc-9
export CXX=g++-9
```

## Build

With `make`:
```
mkdir build && cd build
cmake -G"Unix Makefiles" ..
make
cp src/router/router ../
```

With `ninja`
```
mkdir build && cd build
cmake -GNinja ..
ninja
cp src/router/router ../
```

## Usage

First define a `router.config` containing all available routers in the router with a port and their respective neighbors
e.g.
```
1; 5001; 2,3
2; 5002; 1,3
3; 5003; 1,2,4
4; 5004; 3
```
Layout is: `RouterId; Port; Neighbor1,Neighbor2,...`


The usage for the `router` command is:
```
./router <routerId>
```
where `routerId` is one of the routerIds defined in the `router.config`.


Start multiple routers by typing `./router <id>` in multiple terminal windows.
To send a message, type `send <id> <message> [message]` in stdin of a router process.
To stop the router, use `exit`.